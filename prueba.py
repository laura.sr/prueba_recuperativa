#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

#funcion que lee el archivo json y lo almacena en una variable
def leer_datos():
	with open('iris.json') as file:
		data = json.load(file)
	return data
   
#funcion que extrae el nombre de las especies
def nombres(data):
	l = []
	for i in data:
		for key, valor in i.items():
			if (key == "species"):
				l.append(valor)
	return l
#funcion que calcula el promedio del ancho y largo de los petalos
def petalos(data):
	#se declaran las variables a utilizar
	setosa_largo = []
	setosa_ancho = []
	versicolor_largo = []
	versicolor_ancho = []
	virginica_largo = []
	virginica_ancho = []
	cantidad = cantidad2 = cantidad3 = 0
	promedio_largo = []
	promedio_ancho = []
	
	#se recorre los datos del json
	for i in data:
		for key, valor in i.items():
			if (key == "petalLength"):
				largo = valor
			if (key == "petalWidth"):
				ancho = valor
			if (key == "species"):
				if (valor == "setosa"):
					setosa_largo.append(largo)
					setosa_ancho.append(ancho)
					cantidad = cantidad + 1
				if (valor == "versicolor"):
					versicolor_largo.append(largo)
					versicolor_ancho.append(ancho)
					cantidad2 = cantidad2 + 1
				if (valor == "virginica"):
					virginica_largo.append(largo)
					virginica_ancho.append(ancho)
					cantidad3 = cantidad3 + 1
	#se calculan los promedios para cada especie
	promedio_largo.append(sum(setosa_largo) / cantidad)
	promedio_ancho.append(sum(setosa_ancho) / cantidad)
	promedio_largo.append(sum(versicolor_largo) / cantidad2)
	promedio_ancho.append(sum(versicolor_ancho) / cantidad2)
	promedio_largo.append(sum(virginica_largo) / cantidad3)
	promedio_ancho.append(sum(virginica_ancho) / cantidad3)
	#se imprimen los promedios de las especies
	print("El promedio del largo de los pétalos de setosa es: ", promedio_largo[0], " Y el ancho es: ", promedio_ancho[0])
	print("El promedio del largo de los pétalos de versicolor es: ", promedio_largo[1], " Y el ancho es: ", promedio_ancho[1])
	print("El promedio del largo de los pétalos de virginica es: ", promedio_largo[2], " Y el ancho es: ", promedio_ancho[2])
	return promedio_largo, promedio_ancho
	
#funcion que permite saber cual es el petalo mas largo y ancho
def maximo(promedio_largo, promedio_ancho):
	if (max(promedio_largo) == promedio_largo[0] and max(promedio_ancho) == promedio_ancho[0]):
		print("El promedio más alto de largo y ancho es: ",promedio_largo[0]," y", promedio_ancho[0], " respectivamente de setosa")
	if (max(promedio_largo) == promedio_largo[1] and max(promedio_ancho) == promedio_ancho[1]):
		print("El promedio más alto de largo y ancho es: ",promedio_largo[1]," y", promedio_ancho[1], " respectivamente de versicolor")
	if (max(promedio_largo) == promedio_largo[2] and max(promedio_ancho) == promedio_ancho[2]):
		print("El promedio más alto de largo y ancho es: ",promedio_largo[2]," y", promedio_ancho[2], " respectivamente de virginica")

#función que evalua los registros de los rangos del promedio de cada especie
def registro(promedio_largo, promedio_ancho, data):
	cantidad = cantidad2 = cantidad3 = 0
	for i in data:
		for key, valor in i.items():
			if (key == "petalLength"):
				largo = valor
			if (key == "petalWidth"):
				ancho = valor
			if (key == "species"):
				if ((valor == "setosa") and (promedio_largo[0] + 3 > largo) and (promedio_largo[0] -3 < largo) and (promedio_ancho[0] + 3 > ancho) and (promedio_ancho[0] -3 < ancho)):
					cantidad = cantidad + 1
				if ((valor == "versicolor") and (promedio_largo[1] + 3 > largo) and (promedio_largo[1] -3 < largo) and (promedio_ancho[1] + 3 > ancho) and (promedio_ancho[1] -3 < ancho)):
					cantidad2 = cantidad2 + 1
				if ((valor == "virginica") and (promedio_largo[2] + 3 > largo) and (promedio_largo[2] -3 < largo) and (promedio_ancho[2] + 3 > ancho) and (promedio_ancho[2] -3 < ancho)):
					cantidad3 = cantidad3 + 1
	#se imprimen los rangos de los largos y anchos de cada especie
	print("El rango de largos de setosas es: ", promedio_largo[0] - 3, " - ", promedio_largo[0] + 3)
	print("El rango de anchos de setosas es: ", promedio_ancho[0] - 3, " - ", promedio_ancho[0] + 3)
	print("La cantidad de registros que cumplan la condición en setosas es: ", cantidad)
	print("El rango de largos de versicolor es: ", promedio_largo[1] - 3, " - ", promedio_largo[1] + 3)
	print("El rango de anchos de versicolor es: ", promedio_ancho[1] - 3, " - ", promedio_ancho[1] + 3)
	print("La cantidad de registros que cumplan la condición en versicolor es: ", cantidad2)
	print("El rango de largos de virginica es: ", promedio_largo[2] - 3, " - ", promedio_largo[2] + 3)
	print("El rango de anchos de virginica es: ", promedio_ancho[2] - 3, " - ", promedio_ancho[2] + 3)
	print("La cantidad de registros que cumplan la condición en virginica es: ", cantidad3)
	
#función que evalúa la máxima para el alto de un sepalo 
def sepal(data):
	#se definen las variables a utilizar
	setosa_largo = []
	versicolor_largo = []
	virginica_largo = []
	diccionario = {}
	
	for i in data:
		for key, valor in i.items():
			if (key == "sepalLength"):
				largo = valor
			if (key == "species"):
				if (valor == "setosa"):
					setosa_largo.append(largo)
				if (valor == "versicolor"):
					versicolor_largo.append(largo)
				if (valor == "virginica"):
					virginica_largo.append(largo)
	#se usan los diccionarios para filtrar el sepal con el numero mas alto
	diccionario["setosa"] = max(setosa_largo)
	diccionario["versicolor"] = max(versicolor_largo)
	diccionario["virginica"] = max(virginica_largo)
	clave_mayor = max(diccionario.keys())
	print("El sepal con el numero más alto es: ",diccionario.get(clave_mayor) ," Y es de la especie: ", clave_mayor)
	
#se crean los archivos separados por especies
def generar_archivo(data):	
	tupla = {}
	#se abren y crean los archivos
	archivo = open("setosa.json", "w")
	archivo2 = open("versicolor.json", "w")
	archivo3 = open("virginica.json", "w")
	
	#se recorre el json y se escriben los archivos
	for i in data:
		for key, valor in i.items():
			tupla[key] = valor
			if (key == "species"):
				if (valor == "setosa"):
					archivo.write(str(tupla))
				if (valor == "versicolor"):
					archivo2.write(str(tupla))
				if (valor == "virginica"):
					archivo3.write(str(tupla))
	#se cierran los archivos
	archivo.close()
	archivo2.close()
	archivo3.close()
				
#es la función principal	
if __name__ == "__main__":
	#se declaran las funciones para el funcionamiento del programa
	promedio_largo = promedio_ancho = []
	data = leer_datos()
	l = nombres(data)
	print("species: " + str(set(l)))
	promedio_largo, promedio_ancho = petalos(data)
	maximo(promedio_largo, promedio_ancho)
	registro(promedio_largo, promedio_ancho, data)
	sepal(data)
	generar_archivo(data)
	
